module com.example.life {
    requires javafx.controls;
    requires javafx.fxml;
    requires jbox2d.library;


    opens apps;
    opens controllers;
}