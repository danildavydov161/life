package controllers;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;

public class LifeController implements Initializable {

    private final int WIDTH = 2000;
    private final int HEIGHT = 1000;
    private final double SIZE = 8;
    private final int ROWS = (int) (HEIGHT / SIZE);
    private final int COLS = (int) (WIDTH / SIZE);
    private boolean[][] cells = new boolean[ROWS][COLS];
    private boolean isStarted = false;

    @FXML
    private StackPane mainSP;
    @FXML
    private Canvas cv;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        GraphicsContext gc = cv.getGraphicsContext2D();

        cv.addEventHandler(MouseEvent.MOUSE_DRAGGED, e -> {
            if (!isStarted) {

                int row = (int)(e.getY() / SIZE);
                int col = (int)(e.getX() / SIZE);

                if (row >= 0 && row < cells.length && col >= 0 && col < cells[0].length) {

                    if(e.getButton() == MouseButton.PRIMARY){
                        cells[row][col] = true;
                        gc.setFill(Color.BLACK);
                        gc.fillRect(col * SIZE, row * SIZE, SIZE, SIZE);
                    }

                    else if(e.getButton() == MouseButton.SECONDARY){
                        cells[row][col] = false;
                        gc.setFill(Color.WHITE);
                        gc.fillRect(col * SIZE, row * SIZE, SIZE, SIZE);
                    }


                }

            }
        });

        mainSP.setOnMouseClicked(mouseEvent -> {
            mainSP.requestFocus();
        });

        mainSP.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                isStarted = true;
            }
            else if (event.getCode() == KeyCode.BACK_SPACE) {
                isStarted = false;
            }
            else if (event.getCode() == KeyCode.C) {
                cells = new boolean[ROWS][COLS];
                gc.clearRect(0, 0, cv.getWidth(), cv.getHeight());
                gc.setFill(Color.WHITE);
                gc.fillRect(0, 0, cv.getWidth(), cv.getHeight());
            }
        });

        animation(gc);

    }

    private void animation(GraphicsContext gc) {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {

                drawGrid(gc);
                if (!isStarted) {
                    return;
                }
                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                boolean[][] newCells = new boolean[ROWS][COLS];
                for (int i = 0; i < ROWS; i++) {
                    for (int j = 0; j < COLS; j++) {
                        int blackNeighbors = countBlackNeighbors(i, j);
                        if (cells[i][j]) {
                            if (blackNeighbors < 2 || blackNeighbors > 3) {
                                newCells[i][j] = false;
                            } else {
                                newCells[i][j] = true;
                            }
                        } else {
                            if (blackNeighbors == 3) {
                                newCells[i][j] = true;
                            } else {
                                newCells[i][j] = false;
                            }
                        }
                        double t = blackNeighbors / 8.0; // Пропорция черных соседей (от 0 до 1)
                        Color color;
                        if (cells[i][j]) {
                            color = Color.BLACK.interpolate(Color.WHITE, t);
                        } else {
                            color = Color.WHITE.interpolate(Color.BLACK, t);
                        }
                        gc.setFill(color);
                        gc.fillRect(j * SIZE, i * SIZE, SIZE, SIZE);
                    }
                }
                cells = newCells;
            }
        };
        timer.start();
    }

    private void drawGrid(GraphicsContext gc) {

        gc.setGlobalAlpha(0.8);

        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLS; j++) {
                double x = j * SIZE;
                double y = i * SIZE;
                gc.strokeRect(x, y, SIZE, SIZE);
            }
        }

        for (double x = 0; x < WIDTH; x += SIZE) {
            gc.strokeLine(x, 0, x, HEIGHT);
        }
        for (double y = 0; y < HEIGHT; y += SIZE) {
            gc.strokeLine(0, y, WIDTH, y);
        }

    }

    private int countBlackNeighbors(int row, int col) {
        int count = 0;
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {
                if (i < 0 || i >= ROWS ||
                        j < 0 || j >= COLS ||
                        (i == row && j == col)) {
                    continue;
                }
                if (cells[i][j]) {
                    count++;
                }
            }
        }
        return count;
    }

}