package apps;

import javafx.animation.KeyFrame; import javafx.animation.Timeline; import javafx.application.Application; import javafx.scene.*; import javafx.scene.paint.Color; import javafx.scene.paint.PhongMaterial; import javafx.scene.shape.Box; import javafx.scene.shape.Rectangle; import javafx.scene.transform.Rotate; import javafx.stage.Stage; import javafx.util.Duration; import org.jbox2d.dynamics.World;

public class Figure extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private double mousePosX, mousePosY;
    private double mouseOldX, mouseOldY;
    private final Rotate rotateX = new Rotate(0, Rotate.X_AXIS);
    private final Rotate rotateY = new Rotate(0, Rotate.Y_AXIS);

    private World world;
    // Гравитация мира
    private final double GRAVITY = 9.8;

    @Override
    public void start(Stage primaryStage) {
        // Создаем куб
        Box box = new Box(100, 100, 100);

        // Задаем цвет и материал для куба
        PhongMaterial material = new PhongMaterial();
        material.setDiffuseColor(Color.RED);
        box.setMaterial(material);

        // Создаем группу объектов и добавляем куб в эту группу
        Group root = new Group(box);

        // Создание подсцены
        SubScene subScene = new SubScene(root, 800, 600, true, SceneAntialiasing.BALANCED);
        subScene.setFill(Color.GRAY); // Задаем цвет фона
        subScene.setCamera(new PerspectiveCamera()); // Задаем камеру

        // Создаем пол
        Rectangle ground = new Rectangle(800, 100);
        ground.setFill(Color.GREEN);
        ground.setTranslateX(0);
        ground.setTranslateY(500);

        // Добавление пола в группу объектов
        root.getChildren().add(ground);

        // Создание сцены
        Scene scene = new Scene(new Group(subScene), 800, 600, Color.GRAY);

        // Добавление анимации падения
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0), evt -> {
                }),
                new KeyFrame(Duration.seconds(0.016), evt -> {
                    double ypos = box.translateYProperty().doubleValue();
                    if (ypos + box.getHeight() + GRAVITY <= 500) { // Куб не достиг земли
                        box.setTranslateY(ypos + GRAVITY);
                    }
                })
        );
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();

        // Обработка событий мыши для вращения куба
        subScene.setOnMousePressed(e -> {
            mousePosX = e.getSceneX();
            mousePosY = e.getSceneY();
            mouseOldX = e.getSceneX();
            mouseOldY = e.getSceneY();
        });
        subScene.setOnMouseDragged(e -> {
            mousePosX = e.getSceneX();
            mousePosY = e.getSceneY();
            double deltaX = (mousePosX - mouseOldX);
            double deltaY = (mousePosY - mouseOldY);
            if (e.isPrimaryButtonDown()) {
                rotateX.setAngle(rotateX.getAngle() - (deltaY / subScene.getHeight() * 360));
                rotateY.setAngle(rotateY.getAngle() + (deltaX / subScene.getWidth() * 360));
            }
            mouseOldX = mousePosX;
            mouseOldY = mousePosY;
        });

        // Добавление вращения кубу и задание начальной позиции
        box.getTransforms().addAll(rotateX, rotateY);
        box.setTranslateX(scene.getWidth() / 2 - box.getWidth() / 2);
        box.setTranslateY(scene.getHeight() / 2 - box.getHeight() / 2);

        primaryStage.setTitle("Rotation and animation test");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}